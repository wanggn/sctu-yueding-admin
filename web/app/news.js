var pageIndex = 0;
var pageSize = 3;
var pageTotal = 0;
var tbody = $("#news_list");
$(function () {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
});

function AjaxGetData(index, size) {
    $.ajax({
        url: baseUrl + "/news",
        type: "GET",
        data: {page: index, size: size},
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotal = Math.ceil(recordCount / pageSize);
            $(".first").text("第" + index + "页");
            $(".total").text("共" + pageTotal + "页");
            var data = json.data;
            tbody.empty();
            $.each(data, function (i, news) {
                var tr = "";

                tr += "<tr id='"+news.id+"'>";

                tr += "<td>";
                tr += news.title;
                tr += "</td>";

                tr += "<td>";
                tr += format(news.createDate);
                tr += "</td>";

                tr += "<td>";
                tr += '<a href="javascript:details(\'../T/T_newDetails.html?newId=' + news.id + '\',\'公告详情\',[\'710px\', \'500px\']);">详情</a>';
                tr += "</td>";

                tr += "<td>";
                tr += '<a href="javascript:edit(\'../T/T_editNew.html?newId='+news.id+'\',\'新闻内容修改\')">修改</a>| <a href="javascript:confirmurl(\''+news.id+'\',\'确认要删除吗？\')">删除</a>';
                tr += "</td>";

                tbody.append(tr);
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPage() {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToPrePage() {
    pageIndex -= 1;
    pageIndex = pageIndex >= 1 ? pageIndex : 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToNextPage() {
    if (pageIndex < pageTotal) {
        pageIndex += 1;
    }
    AjaxGetData(pageIndex, pageSize);
}

function GoToEndPage() {
    pageIndex = pageTotal;
    AjaxGetData(pageIndex, pageSize);
}

function GoToAppointPage() {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndex = pageIndex;
        pageIndex = parseInt($(".num>input").val());
        if (pageIndex <= 0 || pageIndex > pageTotal) {
            pageIndex = tempPageIndex;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetData(pageIndex, pageSize);
        }
    }
}
