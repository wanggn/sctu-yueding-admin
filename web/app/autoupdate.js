var pageIndex = 0;
var pageSize = 10;
var pageTotal = 0;
var tbody = $("#app_list");
$(function () {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
});

function AjaxGetData(index, size) {
    $.ajax({
        url: baseUrl + "/appVersions",
        type: "GET",
        data: {page: index, size: size},
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotal = Math.ceil(recordCount / pageSize);
            $(".first").text("第" + index + "页");
            $(".total").text("共" + pageTotal + "页");
            var data = json.data;
            tbody.empty();
            $.each(data, function (i, app) {
                var tr = "";

                tr += "<tr>";

                tr += "<td>";
                tr += app.name;
                tr += "</td>";

                tr += "<td>";
                tr += app.description;
                tr += "</td>";

                tr += "<td>";
                tr += app.versionCode;
                tr += "</td>";

                tr += "<td>";
                tr += app.versionName;
                tr += "</td>";

                tr += "<td>";
                tr += '<a href="'+app.url+'">'+ app.url+'</a>';
                tr += "</td>";

                tr += '<td id="code_' + app.id + '">';
                tr += "</td>";

                tr += "<td>";
                tr += format(app.createTime);
                tr += "</td>";

                tbody.append(tr);

                if (app.url != null) {
                    $('#code_' + app.id + '').qrcode({
                        render: "canvas",//设置渲染方式
                        width: 128,     //设置宽度
                        height: 128,     //设置高度
                        typeNumber: -1,      //计算模式
                        correctLevel    : 0,//纠错等级
                        //correctLevel : QRErrorCorrectLevel.H,//纠错等级
                        background: "#ffffff",//背景颜色
                        foreground: "#000000",//前景颜色
                        text: '' + app.url + ''  //设置二维码内容
                    });
                }
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPage() {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToPrePage() {
    pageIndex -= 1;
    pageIndex = pageIndex >= 1 ? pageIndex : 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToNextPage() {
    if (pageIndex < pageTotal) {
        pageIndex += 1;
    }
    AjaxGetData(pageIndex, pageSize);
}

function GoToEndPage() {
    pageIndex = pageTotal;
    AjaxGetData(pageIndex, pageSize);
}

function GoToAppointPage() {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndex = pageIndex;
        pageIndex = parseInt($(".num>input").val());
        if (pageIndex <= 0 || pageIndex > pageTotal) {
            pageIndex = tempPageIndex;
            alert("请输入正确的页码!");
        }
        else {
            AjaxGetData(pageIndex, pageSize);
        }
    }
}