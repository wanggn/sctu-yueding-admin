var pageIndex = 0;
var pageSize = 3;
var pageTotal = 0;
var tbody = $("#thumbnail_list");
$(function () {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
});

function AjaxGetData(index, size) {
    $.ajax({
        url: v2 + "/thumbnails",
        type: "GET",
        data: {page: index - 1, size: size},
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotal = Math.ceil(recordCount / pageSize);
            $(".first").text("第" + index + "页");
            $(".total").text("共" + pageTotal + "页");
            var data = json.data;
            tbody.empty();
            $.each(data, function (i, thumbnails) {
                var tr = "";

                tr += "<tr id='" + thumbnails.id + "'>";

                tr += "<td>";
                tr += thumbnails.title;
                tr += "</td>";

                if (thumbnails.type == 'match') {
                    tr += "<td>";
                    tr += "比赛";
                    tr += "</td>";
                } else if (thumbnails.type == 'news') {
                    tr += "<td>";
                    tr += "新闻";
                    tr += "</td>";
                }

                tr += "<td>";
                tr += '<a href="' + thumbnails.content + '" target="_blank">详情</a>';
                tr += "</td>";

                //tr += "<td>";
                //tr += '<a href="javascript:details(\'../T/T_thumbnailDetails.html?thumbnailId=' + thumbnails.id + '\',\'轮播图详情\',[\'710px\', \'500px\']);">详情</a>';
                //tr += "</td>";

                tr += "<td>";
                tr += '<a href="javascript:edit(\'../T/T_editThumbnail.html?thumbnailId=' + thumbnails.id + '\',\'新闻内容修改\')">修改</a>| <a href="javascript:confirmurl(\'' + thumbnails.id + '\',\'确认要删除吗？\')">删除</a>';
                tr += "</td>";

                tbody.append(tr);
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPage() {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToPrePage() {
    pageIndex -= 1;
    pageIndex = pageIndex >= 1 ? pageIndex : 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToNextPage() {
    if (pageIndex < pageTotal) {
        pageIndex += 1;
    }
    AjaxGetData(pageIndex, pageSize);
}

function GoToEndPage() {
    pageIndex = pageTotal;
    AjaxGetData(pageIndex, pageSize);
}

function GoToAppointPage() {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndex = pageIndex;
        pageIndex = parseInt($(".num>input").val());
        if (pageIndex <= 0 || pageIndex > pageTotal) {
            pageIndex = tempPageIndex;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetData(pageIndex, pageSize);
        }
    }
}
