var pageIndex = 0;
var pageSize = 10;
var pageTotal = 0;
var tbody = $("#user_list");
$(function () {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
});

function AjaxGetData(index, size) {
    $.ajax({
        url: baseUrl + "/users",
        type: "GET",
        data: {page: index - 1, size: size},
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotal = Math.ceil(recordCount / pageSize);
            $(".first").text("第" + index + "页");
            $(".total").text("共" + pageTotal + "页");
            var data = json.data;
            tbody.empty();
            $.each(data, function (i, user) {
                var tr = "";

                tr += "<tr>";

                tr += "<td>";
                tr += user.nickname;
                tr += "</td>";

                tr += "<td>";
                tr += user.login;
                tr += "</td>";

                tr += "<td>";
                tr += user.password;
                tr += "</td>";

                tr += "<td>";
                tr += user.createTime;
                tr += "</td>";

                tbody.append(tr);
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPage() {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToPrePage() {
    pageIndex -= 1;
    pageIndex = pageIndex >= 1 ? pageIndex : 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToNextPage() {
    if (pageIndex < pageTotal) {
        pageIndex += 1;
    }
    AjaxGetData(pageIndex, pageSize);
}

function GoToEndPage() {
    pageIndex = pageTotal;
    AjaxGetData(pageIndex, pageSize);
}

function GoToAppointPage() {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndex = pageIndex;
        pageIndex = parseInt($(".num>input").val());
        if (pageIndex <= 0 || pageIndex > pageTotal) {
            pageIndex = tempPageIndex;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetData(pageIndex, pageSize);
        }
    }
}