var pageIndex = 0;
var pageSize = 10;
var pageTotal = 0;
var tbody = $("#participant_list");
$(function () {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
});

function AjaxGetData(index, size) {
    $.ajax({
        url:baseUrl +  "/participants",
        type: "GET",
        data: {page: index - 1, size: size},
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotal = Math.ceil(recordCount / pageSize) + 2;
            $(".first").text("第" + index + "页");
            $(".total").text("共" + pageTotal + "页");
            var data = json.data;
            tbody.empty();
            $.each(data, function (i, user) {
                var tr = "";

                tr += "<tr>";

                tr += "<td>";
                tr += user.realName;
                tr += "</td>";

                tr += "<td>";
                tr += user.gender;
                tr += "</td>";

                tr += "<td>";
                tr += user.idNumber;
                tr += "</td>";

                tr += "<td>";
                tr += user.phoneNumber;
                tr += "</td>";


                tr += "<td>";
                tr += format(user.createDate);
                tr += "</td>";

                tr += "<td>";
                tr += user.teamName;
                tr += "</td>";

                tbody.append(tr);
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPage() {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToPrePage() {
    pageIndex -= 1;
    pageIndex = pageIndex >= 1 ? pageIndex : 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToNextPage() {
    if (pageIndex < pageTotal) {
        pageIndex += 1;
    }
    AjaxGetData(pageIndex, pageSize);
}

function GoToEndPage() {
    pageIndex = pageTotal;
    AjaxGetData(pageIndex, pageSize);
}

function GoToAppointPage() {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndex = pageIndex;
        pageIndex = parseInt($(".num>input").val());
        if (pageIndex <= 0 || pageIndex > pageTotal) {
            pageIndex = tempPageIndex;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetData(pageIndex, pageSize);
        }
    }
}


function searchUser() {
    var conditionType = $("#type option:selected").val();
    var value = $(".input-text").val();
    $.getJSON(baseUrl + "/participants/classify", {
            conditionType: conditionType,
            value: value
        },
        function (json) {
            if (json.meta.code == "200") {
                tbody.empty();
                $(".first").text("第" + 1 + "页");
                $(".total").text("共" + 1 + "页");
                $.each(json.data, function (i, item) {
                    var tr = "";
                    tr += "<tr>";

                    tr += "<td>";
                    tr += item.realName;
                    tr += "</td>";

                    tr += "<td>";
                    tr += item.gender;
                    tr += "</td>";

                    tr += "<td>";
                    tr += item.idNumber;
                    tr += "</td>";

                    tr += "<td>";
                    tr += item.phoneNumber;
                    tr += "</td>";

                    tr += "<td>";
                    tr += item.teamName;
                    tr += "</td>";

                    tr += "</tr>";
                    tbody.append(tr);
                });
            }
        })
}