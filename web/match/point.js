var matches = $("#matches");
var routes = $("#routes");
var tbody = $("#point_list");
$(function () {
    $.getJSON(baseUrl + "/matches", function (json) {
        var data = json.data;
        matches.empty();
        matches.append('<option value="all">所有</option>');
        $.each(data, function (i, match) {
            var option = "";
            if (i == 0) {
                option += "<option value='" + match.id + "' selected='selected'>";
                option += match.name;
                option += "</option>";
                matches.append(option);

                $.getJSON(baseUrl + "/matches/" + match.id + "/routes",
                    function (json) {
                        var data = json.data;
                        routes.empty();
                        $.each(data, function (i, route) {
                            var option = "";
                            if (i == 0) {
                                option += "<option value='" + route.id + "' selected='selected'>";
                                option += route.introduction;
                                option += "</option>";
                                routes.append(option);

                                $.getJSON(baseUrl + "/matches/" + match.id + "/routes/" + route.id + "/points",
                                    function (json) {
                                        if (json.meta.code == "200") {
                                            tbody.empty();
                                            var data = json.data;
                                            $.each(data, function (i, point) {
                                                var tr = "";
                                                tr += "<tr id='" + point.id + "'>";

                                                tr += "<td>";
                                                tr += point.match;
                                                tr += "</td>";

                                                tr += "<td>";
                                                tr += point.route;
                                                tr += "</td>";

                                                tr += "<td>";
                                                tr += point.routeType;
                                                tr += "</td>";

                                                tr += "<td>";
                                                tr += point.name;
                                                tr += "</td>";

                                                tr += '<td id="point_' + point.id + '">';
                                                tr += "</td>";

                                                tr += '<td id="mission_' + point.id + '">';
                                                tr += "</td>";

                                                tr += "<td>";
                                                tr += '<a href="javascript:edit(\'../T/T_editPoint.html?id=' + point.id + '\',\'路线\')">修改</a> | <a href="javascript:confirmurl(' + point.id + ',\'确认要删除吗？\')">删除</a>';
                                                tr += "</td>";

                                                tr += "</tr>";

                                                tbody.append(tr);

                                                if (point.pointUrl != null) {
                                                    $('#point_' + point.id + '').qrcode({
                                                        render: "canvas",//设置渲染方式
                                                        width: 128,     //设置宽度
                                                        height: 128,     //设置高度
                                                        typeNumber: -1,      //计算模式
                                                        correctLevel: 0,//纠错等级
                                                        //correctLevel : QRErrorCorrectLevel.H,//纠错等级
                                                        background: "#ffffff",//背景颜色
                                                        foreground: "#000000",//前景颜色
                                                        text: '' + point.pointUrl + ''  //设置二维码内容
                                                    });
                                                }

                                                if (point.missionUrl != null) {
                                                    $('#mission_' + point.id + '').qrcode({
                                                        render: "canvas",//设置渲染方式
                                                        width: 128,     //设置宽度
                                                        height: 128,     //设置高度
                                                        typeNumber: -1,      //计算模式
                                                        correctLevel: 0,//纠错等级
                                                        //correctLevel : QRErrorCorrectLevel.H,//纠错等级
                                                        background: "#ffffff",//背景颜色
                                                        foreground: "#000000",//前景颜色
                                                        text: '' + point.missionUrl + ''  //设置二维码内容
                                                    });
                                                }
                                            });
                                        } else {
                                            tbody.empty();
                                            tbody.append("<tr><td  style='text-align: center' colspan='7'>该路线下没有点标！</td></tr>");
                                        }
                                    });

                            } else {
                                option += "<option value='" + route.id + "'>";
                                option += route.introduction;
                                option += "</option>";
                                routes.append(option);
                            }
                        })
                    });
            } else {
                option += "<option value='" + match.id + "'>";
                option += match.name;
                option += "</option>";
                matches.append(option);
            }
        })
    });

    matches.change(function () {
        var matchId = $("#matches option:selected").val();
        if (matchId == "all") {
            window.location.reload();
        } else {
            $.getJSON(baseUrl + "/matches/" + matchId + "/routes",
                function (json) {
                    var data = json.data;
                    routes.empty();
                    $.each(data, function (i, item) {
                        var option = "";
                        if (i == 0) {
                            option += "<option value='" + item.id + "' selected='selected'>";
                            option += item.introduction;
                            option += "</option>";
                            routes.append(option);

                            $.getJSON(baseUrl + "/matches/" + matchId + "/routes/" + item.id + "/points",
                                function (json) {
                                    if (json.meta.code == "200") {
                                        tbody.empty();
                                        var data = json.data;
                                        $.each(data, function (i, point) {
                                            var tr = "";
                                            tr += "<tr id='" + point.id + "'>";

                                            tr += "<td>";
                                            tr += point.match;
                                            tr += "</td>";

                                            tr += "<td>";
                                            tr += point.route;
                                            tr += "</td>";

                                            tr += "<td>";
                                            tr += point.routeType;
                                            tr += "</td>";

                                            tr += "<td>";
                                            tr += point.name;
                                            tr += "</td>";

                                            tr += '<td id="point_' + point.id + '">';
                                            tr += "</td>";

                                            tr += '<td id="mission_' + point.id + '">';
                                            tr += "</td>";

                                            tr += "<td>";
                                            tr += '<a href="javascript:edit(\'../T/T_editPoint.html?id=' + point.id + '\',\'路线\')">修改</a> | <a href="javascript:confirmurl(' + point.id + ',\'确认要删除吗？\')">删除</a>';
                                            tr += "</td>";

                                            tr += "</tr>";

                                            tbody.append(tr);

                                            if (point.pointUrl != null) {
                                                $('#point_' + point.id + '').qrcode({
                                                    render: "canvas",//设置渲染方式
                                                    width: 128,     //设置宽度
                                                    height: 128,     //设置高度
                                                    typeNumber: -1,      //计算模式
                                                    correctLevel: 0,//纠错等级
                                                    //correctLevel : QRErrorCorrectLevel.H,//纠错等级
                                                    background: "#ffffff",//背景颜色
                                                    foreground: "#000000",//前景颜色
                                                    text: '' + point.pointUrl + ''  //设置二维码内容
                                                });
                                            }

                                            if (point.missionUrl != null) {
                                                $('#mission_' + point.id + '').qrcode({
                                                    render: "canvas",//设置渲染方式
                                                    width: 128,     //设置宽度
                                                    height: 128,     //设置高度
                                                    typeNumber: -1,      //计算模式
                                                    correctLevel: 0,//纠错等级
                                                    //correctLevel : QRErrorCorrectLevel.H,//纠错等级
                                                    background: "#ffffff",//背景颜色
                                                    foreground: "#000000",//前景颜色
                                                    text: '' + point.missionUrl + ''  //设置二维码内容
                                                });
                                            }
                                        });
                                    } else {
                                        tbody.empty();
                                        tbody.append("<tr><td  style='text-align: center' colspan='7'>该路线下没有点标！</td></tr>");
                                    }
                                });
                        } else {
                            option += "<option value='" + item.id + "'>";
                            option += item.introduction;
                            option += "</option>";
                            routes.append(option);
                        }

                    })
                })
        }
    });

    routes.change(function () {
        var matchId = $("#matches option:selected").val();
        var routeId = $("#routes option:selected").val();
        $.getJSON(baseUrl + "/matches/" + matchId + "/routes/" + routeId + "/points",
            function (json) {
                if (json.meta.code == "200") {
                    tbody.empty();
                    var data = json.data;
                    $.each(data, function (i, item) {
                        var tr = "";
                        tr += "<tr id='" + item.id + "'>";

                        tr += "<td>";
                        tr += item.match;
                        tr += "</td>";

                        tr += "<td>";
                        tr += item.route;
                        tr += "</td>";

                        tr += "<td>";
                        tr += item.routeType;
                        tr += "</td>";

                        tr += "<td>";
                        tr += item.name;
                        tr += "</td>";

                        tr += '<td id="point_' + item.id + '" >';
                        tr += "</td>";


                        tr += '<td id="mission_' + item.id + '">';
                        tr += "</td>";

                        tr += "<td>";
                        tr += '<a href="javascript:edit(\'../T/T_editPoint.html?id=' + item.id + '\',\'路线\')">修改</a> | <a href="javascript:confirmurl(' + item.id + ',\'确认要删除吗？\')">删除</a>';
                        tr += "</td>";

                        tr += "</tr>";

                        tbody.append(tr);
                        if (item.pointUrl != null) {
                            $('#point_' + item.id + '').qrcode({
                                render: "canvas",//设置渲染方式
                                width: 128,     //设置宽度
                                height: 128,     //设置高度
                                typeNumber: -1,      //计算模式
                                correctLevel: 0,//纠错等级
                                //correctLevel : QRErrorCorrectLevel.H,//纠错等级
                                background: "#ffffff",//背景颜色
                                foreground: "#000000",//前景颜色
                                text: '' + item.pointUrl + ''  //设置二维码内容
                            });
                        }
                        if (item.missionUrl != null) {
                            $('#mission_' + item.id + '').qrcode({
                                render: "canvas",//设置渲染方式
                                width: 128,     //设置宽度
                                height: 128,     //设置高度
                                typeNumber: -1,      //计算模式
                                correctLevel: 0,//纠错等级
                                //correctLevel : QRErrorCorrectLevel.H,//纠错等级
                                background: "#ffffff",//背景颜色
                                foreground: "#000000",//前景颜色
                                text: '' + item.missionUrl + ''  //设置二维码内容
                            });
                        }
                    });
                } else {
                    tbody.empty();
                    tbody.append("<tr><td  style='text-align: center' colspan='7'>该路线下没有点标！</td></tr>");
                }
            });
    });

});

