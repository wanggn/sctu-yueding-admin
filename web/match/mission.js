var matches = $("#matches");
var routes = $("#routes");
var points = $("#points");
var tbody = $("#mission_list");
$(function () {
    $.getJSON(baseUrl + "/matches", function (json) {
        var data = json.data;
        matches.empty();
        matches.append('<option value="all">不限</option>');
        $.each(data, function (i, match) {
            var option = "";
            if (i == 0) {
                option += "<option value='" + match.id + "' selected='selected'>";
                option += match.name;
                option += "</option>";
                matches.append(option);

                $.getJSON(baseUrl + "/matches/" + match.id + "/routes",
                    function (json) {
                        var data = json.data;
                        routes.empty();
                        $.each(data, function (i, route) {
                            var option = "";
                            if (i == 0) {
                                option += "<option value='" + route.id + "' selected='selected'>";
                                option += route.introduction;
                                option += "</option>";
                                routes.append(option);

                                $.getJSON(baseUrl + "/matches/" + match.id + "/routes/" + route.id + "/points",
                                    function (json) {
                                        var data = json.data;
                                        points.empty();
                                        var option = "";
                                        option += "<option value='all' selected='selected'>";
                                        option += "所有";
                                        option += "</option>";
                                        points.append(option);
                                        $.each(data, function (i, point) {
                                            var option = '';
                                            if (i == 0) {
                                                option += "<option value='" + point.id + "'>";
                                                option += point.name;
                                                option += "</option>";
                                                points.append(option);
                                                $.getJSON(baseUrl + "/matches/" + match.id + "/routes/" + route.id + "/missions", function (json) {
                                                    if (json.meta.code == "200") {
                                                        tbody.empty();
                                                        var data = json.data;
                                                        if (data == "" || data == null) {
                                                            tbody.append("<tr><td  style='text-align: center' colspan='6'>没有相关任务数据！</td></tr>")
                                                        } else {
                                                            $.each(data, function (i, item) {
                                                                var tr = "";
                                                                tr += "<tr id='" + item.id + "'>";

                                                                tr += "<td>";
                                                                tr += item.match;
                                                                tr += "</td>";

                                                                tr += "<td>";
                                                                tr += item.route;
                                                                tr += "</td>";

                                                                tr += "<td>";
                                                                tr += item.point;
                                                                tr += "</td>";

                                                                tr += "<td>";
                                                                tr += item.name;
                                                                tr += "</td>";

                                                                tr += "<td width='600px' height='100px'>";
                                                                tr += item.introduction;
                                                                tr += "</td>";

                                                                tr += "<td>";
                                                                tr += '<a href="javascript:edit(\'../T/T_editMission.html?id=' + item.id + '\',\'路线\')">修改</a> | <a href="javascript:confirmurl(' + item.id + ',\'确认要删除吗？\')">删除</a>';
                                                                tr += "</td>";

                                                                tr += "</tr>";

                                                                tbody.append(tr)
                                                            });
                                                        }
                                                    }
                                                })

                                            } else {
                                                option += "<option value='" + point.id + "'>";
                                                option += point.name;
                                                option += "</option>";
                                                points.append(option);
                                            }
                                        })
                                    })

                            } else {
                                option += "<option value='" + route.id + "'>";
                                option += route.introduction;
                                option += "</option>";
                                routes.append(option);
                            }
                        })
                    });
            } else {
                option += "<option value='" + match.id + "'>";
                option += match.name;
                option += "</option>";
                matches.append(option);
            }
        })
    });
});


matches.change(function () {
    var matchId = $("#matches option:selected").val();
    if (matchId == "all") {
        window.location.reload();
    } else {
        $.getJSON(baseUrl + "/matches/" + matchId + "/routes", function (json) {
            routes.empty();
            $.each(json.data, function (i, route) {
                var option = "";

                if (i == 0) {
                    option += "<option value='" + route.id + "' selected='selected'>";
                    option += route.introduction;
                    option += "</option>";
                    routes.append(option);

                    $.getJSON(baseUrl + "/matches/" + matchId + "/routes/" + route.id + "/missions", function (json) {
                        if (json.meta.code == "200") {
                            tbody.empty();
                            var data = json.data;
                            if (data == "" || data == null) {
                                tbody.append("<tr><td  style='text-align: center' colspan='6'>没有相关任务数据！</td></tr>")
                            } else {
                                $.each(data, function (i, item) {
                                    var tr = "";
                                    tr += "<tr id='" + item.id + "'>";

                                    tr += "<td>";
                                    tr += item.match;
                                    tr += "</td>";

                                    tr += "<td>";
                                    tr += item.route;
                                    tr += "</td>";

                                    tr += "<td>";
                                    tr += item.point;
                                    tr += "</td>";

                                    tr += "<td>";
                                    tr += item.name;
                                    tr += "</td>";

                                    tr += "<td width='600px' height='100px'>";
                                    tr += item.introduction;
                                    tr += "</td>";

                                    tr += "<td>";
                                    tr += '<a href="javascript:edit(\'../T/T_editMission.html?id=' + item.id + '\',\'路线\')">修改</a> | <a href="javascript:confirmurl(' + item.id + ',\'确认要删除吗？\')">删除</a>';
                                    tr += "</td>";

                                    tr += "</tr>";

                                    tbody.append(tr)
                                });
                            }
                        }
                    });

                    $.getJSON(baseUrl + "/matches/" + matchId + "/routes/" + route.id + "/points",
                        function (json) {
                            var option = "";
                            points.empty();
                            option += "<option value='all'>";
                            option += "所有";
                            option += "</option>";
                            points.append(option);
                            $.each(json.data, function (i, point) {
                                var option = "";
                                option += "<option value='" + point.id + "'>";
                                option += point.name;
                                option += "</option>";
                                points.append(option);
                            })
                        });

                } else {
                    option += "<option value='" + route.id + "'>";
                    option += route.introduction;
                    option += "</option>";
                    routes.append(option);
                }

            })
        })
    }
});

routes.change(function () {
    var matchId = $("#matches option:selected").val();
    var routeId = $("#routes option:selected").val();

    $.getJSON(baseUrl + "/matches/" + matchId + "/routes/" + routeId + "/missions", function (json) {
        if (json.meta.code == "200") {
            tbody.empty();
            var data = json.data;
            if (data == "" || data == null) {
                tbody.append("<tr><td  style='text-align: center' colspan='6'>没有相关任务数据！</td></tr>")
            } else {
                $.each(data, function (i, item) {
                    var tr = "";
                    tr += "<tr id='" + item.id + "'>";

                    tr += "<td>";
                    tr += item.match;
                    tr += "</td>";

                    tr += "<td>";
                    tr += item.route;
                    tr += "</td>";

                    tr += "<td>";
                    tr += item.point;
                    tr += "</td>";

                    tr += "<td>";
                    tr += item.name;
                    tr += "</td>";

                    tr += "<td width='600px' height='100px'>";
                    tr += item.introduction;
                    tr += "</td>";

                    tr += "<td>";
                    tr += '<a href="javascript:edit(\'../T/T_editMission.html?id=' + item.id + '\',\'路线\')">修改</a> | <a href="javascript:confirmurl(' + item.id + ',\'确认要删除吗？\')">删除</a>';
                    tr += "</td>";

                    tr += "</tr>";

                    tbody.append(tr)
                });
            }
        }
    })

    $.getJSON(baseUrl + "/matches/" + matchId + "/routes/" + routeId + "/points",
        function (json) {
            var option = "";
            points.empty();
            option += "<option value='all'>";
            option += "所有";
            option += "</option>";
            points.append(option);
            $.each(json.data, function (i, point) {
                var option = "";
                option += "<option value='" + point.id + "'>";
                option += point.name;
                option += "</option>";
                points.append(option);
            })
        });
});


points.change(function () {
    var matchId = $("#matches option:selected").val();
    var routeId = $("#routes option:selected").val();
    var pointId = $("#points option:selected").val();
    if (pointId == "all") {
        $.getJSON(baseUrl + "/matches/" + matchId + "/routes/" + routeId + "/missions", function (json) {
            if (json.meta.code == "200") {
                tbody.empty();
                var data = json.data;
                if (data == "" || data == null) {
                    tbody.append("<tr><td  style='text-align: center' colspan='6'>没有相关任务数据！</td></tr>")
                } else {
                    $.each(data, function (i, item) {
                        var tr = "";
                        tr += "<tr id='" + item.id + "'>";

                        tr += "<td>";
                        tr += item.match;
                        tr += "</td>";

                        tr += "<td>";
                        tr += item.route;
                        tr += "</td>";

                        tr += "<td>";
                        tr += item.point;
                        tr += "</td>";

                        tr += "<td>";
                        tr += item.name;
                        tr += "</td>";

                        tr += "<td width='600px' height='100px'>";
                        tr += item.introduction;
                        tr += "</td>";

                        tr += "<td>";
                        tr += '<a href="javascript:edit(\'../T/T_editMission.html?id=' + item.id + '\',\'路线\')">修改</a> | <a href="javascript:confirmurl(' + item.id + ',\'确认要删除吗？\')">删除</a>';
                        tr += "</td>";

                        tr += "</tr>";

                        tbody.append(tr)
                    });
                }
            }
        })
    } else {
        $.getJSON(baseUrl + "/matches/" + matchId + "/routes/" + routeId + "/points/" + pointId + "/missions",
            function (json) {
                if (json.meta.code == "200") {
                    tbody.empty();
                    var data = json.data;
                    if (data == "" || data == null) {
                        tbody.append("<tr><td  style='text-align: center' colspan='6'>该点标下没有任务！</td></tr>")
                    } else {
                        $.each(data, function (i, item) {

                            var tr = "";
                            tr += "<tr id='" + item.id + "'>";

                            tr += "<td>";
                            tr += item.match;
                            tr += "</td>";

                            tr += "<td>";
                            tr += item.route;
                            tr += "</td>";

                            tr += "<td>";
                            tr += item.point;
                            tr += "</td>";

                            tr += "<td>";
                            tr += item.name;
                            tr += "</td>";

                            tr += "<td width='600px' height='100px'>";
                            tr += item.introduction;
                            tr += "</td>";

                            tr += "<td>";
                            tr += '<a href="javascript:edit(\'../T/T_editMission.html?id=' + item.id + '\',\'路线\')">修改</a> | <a href="javascript:confirmurl(' + item.id + ',\'确认要删除吗？\')">删除</a>';
                            tr += "</td>";

                            tr += "</tr>";

                            tbody.append(tr)
                        });
                    }
                }
            });

    }
});