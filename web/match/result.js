var matches = $("#matches");
var routes = $("#routes");
var tbody = $("#result_list");
$(function () {
    $.getJSON(baseUrl + "/matches", function (json) {
        var data = json.data;
        matches.empty();
        matches.append('<option value="all">不限</option>');
        $.each(data, function (i, match) {
            var option = "";
            if (i == 0) {
                option += "<option value='" + match.id + "' selected='selected'>";
                option += match.name;
                option += "</option>";
                matches.append(option);

                $.getJSON(baseUrl + "/matches/" + match.id + "/routes",
                    function (json) {
                        var data = json.data;
                        routes.empty();
                        $.each(data, function (i, route) {
                            var option = "";
                            if (i == 0) {
                                option += "<option value='" + route.id + "' selected='selected'>";
                                option += route.introduction;
                                option += "</option>";
                                routes.append(option);

                                $.getJSON(baseUrl + "/matches/" + match.id + "/routes/" + route.id + "/result", function (json) {
                                    if (json.meta.code == "200") {
                                        tbody.empty();
                                        var data = json.data;
                                        $.each(data, function (i, item) {
                                            var tr = "";
                                            tr += "<tr>";

                                            tr += "<td>";
                                            tr += item.match;
                                            tr += "</td>";

                                            tr += "<td>";
                                            tr += item.route;
                                            tr += "</td>";

                                            tr += "<td>";
                                            tr += item.team;
                                            tr += "</td>";

                                            tr += "<td>";
                                            tr += item.time;
                                            tr += "</td>";

                                            tr += "<td>";
                                            tr += item.isComplete;
                                            tr += "</td>";

                                            tr += "<td>";
                                            tr += item.remark;
                                            tr += "</td>";

                                            tr += "<td>";
                                            tr += item.rank;
                                            tr += "</td>";

                                            tr += "</tr>";

                                            tbody.append(tr)
                                        });
                                    } else {
                                        tbody.empty();
                                        tbody.append("<tr><td  style='text-align: center' colspan='7'>该路线下没有成绩数据！</td></tr>");
                                    }
                                })

                            } else {
                                option += "<option value='" + route.id + "'>";
                                option += route.introduction;
                                option += "</option>";
                                routes.append(option);
                            }
                        })
                    });
            } else {
                option += "<option value='" + match.id + "'>";
                option += match.name;
                option += "</option>";
                matches.append(option);
            }
        })
    });
});


matches.change(function () {
    var matchId = $("#matches option:selected").val();
    if (matchId == "all") {
        window.location.reload();
    } else {
        $.getJSON(baseUrl + "/matches/" + matchId + "/routes", function (json) {
            var data = json.data;
            routes.empty();
            $.each(data, function (i, route) {
                var option = "";
                if(i == 0){
                    option += "<option value='" + route.id + "' selected='selected'>";
                    option += route.introduction;
                    option += "</option>";
                    routes.append(option);

                    $.getJSON(baseUrl + "/matches/" + matchId + "/routes/" + route.id + "/result", function (json) {
                        if (json.meta.code == "200") {
                            tbody.empty();
                            var data = json.data;
                            $.each(data, function (i, item) {
                                var tr = "";
                                tr += "<tr>";

                                tr += "<td>";
                                tr += item.match;
                                tr += "</td>";

                                tr += "<td>";
                                tr += item.route;
                                tr += "</td>";

                                tr += "<td>";
                                tr += item.team;
                                tr += "</td>";

                                tr += "<td>";
                                tr += item.time;
                                tr += "</td>";

                                tr += "<td>";
                                tr += item.isComplete;
                                tr += "</td>";

                                tr += "<td>";
                                tr += item.remark;
                                tr += "</td>";

                                tr += "<td>";
                                tr += item.rank;
                                tr += "</td>";

                                tr += "</tr>";

                                tbody.append(tr)
                            });
                        } else {
                            tbody.empty();
                            tbody.append("<tr><td  style='text-align: center' colspan='7'>该路线下没有成绩数据！</td></tr>");
                        }
                    })
                }else {
                    option += "<option value='" + route.id + "'>";
                    option += route.introduction;
                    option += "</option>";
                    routes.append(option);
                }

            })
        })
    }
});

routes.change(function () {
    var matchId = $("#matches option:selected").val();
    var routeId = $("#routes option:selected").val();
    $.getJSON(baseUrl + "/matches/" + matchId + "/routes/" + routeId + "/result", function (json) {
        if (json.meta.code == "200") {
            tbody.empty();
            var data = json.data;
            $.each(data, function (i, item) {
                var tr = "";
                tr += "<tr>";

                tr += "<td>";
                tr += item.match;
                tr += "</td>";

                tr += "<td>";
                tr += item.route;
                tr += "</td>";

                tr += "<td>";
                tr += item.team;
                tr += "</td>";

                tr += "<td>";
                tr += item.time;
                tr += "</td>";

                tr += "<td>";
                tr += item.isComplete;
                tr += "</td>";

                tr += "<td>";
                tr += item.remark;
                tr += "</td>";

                tr += "<td>";
                tr += item.rank;
                tr += "</td>";

                tr += "</tr>";

                tbody.append(tr)
            });
        } else {
            tbody.empty();
            tbody.append("<tr><td  style='text-align: center' colspan='7'>该路线下没有成绩数据！</td></tr>");
        }
    })
});