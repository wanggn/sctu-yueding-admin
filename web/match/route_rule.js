var pageIndex = 0;
var pageSize = 10;
var pageTotal = 0;
var tbody = $("#routeRule_list");
$(function () {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
    $.getJSON(baseUrl + "/matches", function (json) {
        var data = json.data;
        var matches = $("#matches");
        matches.empty();
        matches.append('<option value="all">不限</option>');
        $.each(data, function (i, item) {
            matches.append("<option value='" + item.id + "'>" + item.name + "</option>")
        })
    })
});

$("#matches").change(function () {
    var matchId = $("#matches option:selected").val();
    if (matchId == "all") {
        AjaxGetData(pageIndex, pageSize);
    } else {
        $.getJSON(baseUrl + "/matches/" + matchId + "/matchRules",
            function (json) {
                if (json.meta.code == "200") {
                    tbody.empty();
                    var data = json.data;
                    $.each(data, function (i, rule) {
                        var tr = "";
                        tr += "<tr id='" + rule.id + "'>";

                        tr += "<td>";
                        tr += rule.match;
                        tr += "</td>";

                        tr += "<td>";
                        tr += rule.route;
                        tr += "</td>";

                        tr += "<td>";
                        tr += rule.ruleKey;
                        tr += "</td>";

                        tr += "<td>";
                        tr += rule.ruleValue;
                        tr += "</td>";

                        tr += "<td>";
                        tr += '<a href="javascript:edit(\'../T/T_editRouteRule.html?id=' + rule.id + '\',\'路线\')">修改</a> | <a href="javascript:confirmurl(' + rule.id + ',\'确认要删除吗？\')">删除</a>';
                        tr += "</td>";

                        tr += "</tr>";

                        tbody.append(tr)
                    });
                }
            })

    }

});


function AjaxGetData(index, size) {
    $.ajax({
        url: baseUrl + "/matchRules",
        type: "GET",
        data: {page: index - 1 , size: size},
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotal = Math.ceil(recordCount / pageSize);
            $(".first").text("第" + index + "页");
            $(".total").text("共" + pageTotal + "页");
            var data = json.data;
            tbody.empty();

            $.each(data, function (i, rule) {
                var tr = "";

                tr += "<tr id='" + rule.id + "'>";

                tr += "<td>";
                tr += rule.match;
                tr += "</td>";

                tr += "<td>";
                tr += rule.route;
                tr += "</td>";

                tr += "<td>";
                tr += rule.ruleKey;
                tr += "</td>";

                tr += "<td>";
                tr += rule.ruleValue;
                tr += "</td>";

                tr += "<td>";
                tr += '<a href="javascript:edit(\'../T/T_editRouteRule.html?id=' + rule.id + '\',\'路线\')">修改</a> | <a href="javascript:confirmurl(' + rule.id + ',\'确认要删除吗？\')">删除</a>';
                tr += "</td>";

                tbody.append(tr);
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPage() {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToPrePage() {
    pageIndex -= 1;
    pageIndex = pageIndex >= 1 ? pageIndex : 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToNextPage() {
    if (pageIndex < pageTotal) {
        pageIndex += 1;
    }
    AjaxGetData(pageIndex, pageSize);
}

function GoToEndPage() {
    pageIndex = pageTotal;
    AjaxGetData(pageIndex, pageSize);
}

function GoToAppointPage() {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndex = pageIndex;
        pageIndex = parseInt($(".num>input").val());
        if (pageIndex <= 0 || pageIndex > pageTotal) {
            pageIndex = tempPageIndex;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetData(pageIndex, pageSize);
        }
    }
}