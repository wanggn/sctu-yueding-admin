var matches = $("#matches");
var routes = $("#routes");
var teams = $("#teams");
var tbody = $("#procedure_list");

$(function () {
    $.getJSON(baseUrl + "/matches", function (json) {
        var data = json.data;
        matches.empty();
        matches.append('<option value="all">不限</option>');
        $.each(data, function (i, match) {
            var option = "";
            if (i == 0) {
                option += "<option value='" + match.id + "' selected='selected'>";
                option += match.name;
                option += "</option>";
                matches.append(option);

                $.getJSON(baseUrl + "/matches/" + match.id + "/routes",
                    function (json) {
                        var data = json.data;
                        routes.empty();
                        $.each(data, function (i, route) {
                            var option = "";
                            if (i == 0) {
                                option += "<option value='" + route.id + "' selected='selected'>";
                                option += route.introduction;
                                option += "</option>";
                                routes.append(option);

                                $.getJSON(baseUrl + "/matches/" + match.id + "/route/" + route.id + "/teams",
                                    function (json) {
                                        var data = json.data;
                                        teams.empty();
                                        var option = '';
                                        option += "<option value='all' selected='selected'>";
                                        option += "所有";
                                        option += "</option>";
                                        teams.append(option);

                                        $.each(data, function (i, team) {
                                            var option = "";
                                            if (i == 0) {
                                                option += "<option value='" + team.id + "''>";
                                                option += team.name;
                                                option += "</option>";
                                                teams.append(option);

                                                $.getJSON(baseUrl + "/matches/" + match.id + "/routes/" + route.id + "/procedures",
                                                    function (json) {
                                                        if (json.meta.code == "200") {
                                                            tbody.empty();
                                                            var data = json.data;
                                                            if (data == "" || data == null) {
                                                                tbody.append("<tr><td  style='text-align: center' colspan='6'>该团队没有任何扫码记录！</td></tr>")
                                                            } else {
                                                                $.each(data, function (i, item) {

                                                                    var tr = "";
                                                                    tr += "<tr>";

                                                                    tr += "<td>";
                                                                    tr += item.match;
                                                                    tr += "</td>";

                                                                    tr += "<td>";
                                                                    tr += item.route;
                                                                    tr += "</td>";

                                                                    tr += "<td>";
                                                                    tr += item.point;
                                                                    tr += "</td>";

                                                                    tr += "<td>";
                                                                    tr += item.team;
                                                                    tr += "</td>";

                                                                    tr += "<td>";
                                                                    tr += item.content;
                                                                    tr += "</td>";

                                                                    tr += "<td>";
                                                                    tr += format(item.scanTime);
                                                                    tr += "</td>";

                                                                    tr += "</tr>";

                                                                    tbody.append(tr)
                                                                });
                                                            }
                                                        } else {
                                                            tbody.empty();
                                                            tbody.append("<tr><td  style='text-align: center' colspan='6'>没有任何扫码记录！</td></tr>")
                                                        }
                                                    })

                                            } else {
                                                option += "<option value='" + team.id + "'>";
                                                option += team.name;
                                                option += "</option>";
                                                teams.append(option);
                                            }
                                        })
                                    })

                            } else {
                                option += "<option value='" + route.id + "'>";
                                option += route.introduction;
                                option += "</option>";
                                routes.append(option);
                            }
                        })
                    });
            } else {
                option += "<option value='" + match.id + "'>";
                option += match.name;
                option += "</option>";
                matches.append(option);
            }
        })
    });
});


matches.change(function () {
    var matchId = $("#matches option:selected").val();
    if (matchId == "all") {
        window.location.reload();
    } else {
        $.getJSON(baseUrl + "/matches/" + matchId + "/routes", function (json) {
            var data = json.data;
            routes.empty();
            $.each(data, function (i, route) {
                var option = "";

                if(i == 0){
                    option += "<option value='" + route.id + "' selected='selected'>";
                    option += route.introduction;
                    option += "</option>";
                    routes.append(option);

                    $.getJSON(baseUrl + "/matches/" + matchId + "/route/" + route.id + "/teams",
                        function (json) {
                            teams.empty();
                            var option = "";
                            option += "<option value='all' selected='selected'>";
                            option += "所有";
                            option += "</option>";
                            teams.append(option);
                            $.each(json.data, function (i, team) {
                                var option = "";
                                option += "<option value='" + team.id + "'>";
                                option += team.name;
                                option += "</option>";
                                teams.append(option);
                            })
                        });

                }else {
                    option += "<option value='" + route.id + "'>";
                    option += route.introduction;
                    option += "</option>";
                    routes.append(option);
                }

            })
        })
    }
});

routes.change(function () {
    var matchId = $("#matches option:selected").val();
    var routeId = $("#routes option:selected").val();

    $.getJSON(baseUrl + "/matches/" + matchId + "/routes/" + routeId + "/procedures",
        function (json) {
            if (json.meta.code == "200") {
                tbody.empty();
                var data = json.data;
                if (data == "" || data == null) {
                    tbody.append("<tr><td  style='text-align: center' colspan='6'>该团队没有任何扫码记录！</td></tr>")
                } else {
                    $.each(data, function (i, item) {

                        var tr = "";
                        tr += "<tr>";

                        tr += "<td>";
                        tr += item.match;
                        tr += "</td>";

                        tr += "<td>";
                        tr += item.route;
                        tr += "</td>";

                        tr += "<td>";
                        tr += item.point;
                        tr += "</td>";

                        tr += "<td>";
                        tr += item.team;
                        tr += "</td>";

                        tr += "<td>";
                        tr += item.content;
                        tr += "</td>";

                        tr += "<td>";
                        tr += format(item.scanTime);
                        tr += "</td>";

                        tr += "</tr>";

                        tbody.append(tr)
                    });
                }
            } else {
                tbody.empty();
                tbody.append("<tr><td  style='text-align: center' colspan='6'>没有任何扫码记录！</td></tr>")
            }
        });

    $.getJSON(baseUrl + "/matches/" + matchId + "/route/" + routeId + "/teams",
        function (json) {
            teams.empty();
            var option = "";
            option += "<option value='all' selected='selected'>";
            option += "所有";
            option += "</option>";
            teams.append(option);
            $.each(json.data, function (i, team) {
                var option = "";
                option += "<option value='" + team.id + "'>";
                option += team.name;
                option += "</option>";
                teams.append(option);
            })
        });
});


teams.change(function () {
    var matchId = $("#matches option:selected").val();
    var routeId = $("#routes option:selected").val();
    var teamId = $("#teams option:selected").val();
    if (teamId == "all") {
        $.getJSON(baseUrl + "/matches/" + matchId + "/routes/" + routeId + "/procedures",
            function (json) {
                if (json.meta.code == "200") {
                    tbody.empty();
                    var data = json.data;
                    if (data == "" || data == null) {
                        tbody.append("<tr><td  style='text-align: center' colspan='6'>该团队没有任何扫码记录！</td></tr>")
                    } else {
                        $.each(data, function (i, item) {

                            var tr = "";
                            tr += "<tr>";

                            tr += "<td>";
                            tr += item.match;
                            tr += "</td>";

                            tr += "<td>";
                            tr += item.route;
                            tr += "</td>";

                            tr += "<td>";
                            tr += item.point;
                            tr += "</td>";

                            tr += "<td>";
                            tr += item.team;
                            tr += "</td>";

                            tr += "<td>";
                            tr += item.content;
                            tr += "</td>";

                            tr += "<td>";
                            tr += format(item.scanTime);
                            tr += "</td>";

                            tr += "</tr>";

                            tbody.append(tr)
                        });
                    }
                } else {
                    tbody.empty();
                    tbody.append("<tr><td  style='text-align: center' colspan='6'>没有任何扫码记录！</td></tr>")
                }
            })
    } else {
        $.getJSON(baseUrl + "/matches/" + matchId + "/routes/" + routeId + "/teams/" + teamId + "/procedures",
            function (json) {
                if (json.meta.code == "200") {
                    tbody.empty();
                    var data = json.data;
                    if (data == "" || data == null) {
                        tbody.append("<tr><td  style='text-align: center' colspan='6'>该团队没有任何扫码记录！</td></tr>")
                    } else {
                        $.each(data, function (i, item) {
                            //var activities = [];
                            var tr = "";
                            tr += "<tr>";

                            tr += "<td>";
                            tr += item.match;
                            tr += "</td>";

                            tr += "<td>";
                            tr += item.route;
                            tr += "</td>";

                            tr += "<td>";
                            tr += item.point;
                            tr += "</td>";

                            tr += "<td>";
                            tr += item.team;
                            tr += "</td>";

                            tr += "<td>";
                            tr += item.content;
                            tr += "</td>";

                            tr += "<td>";
                            tr += format(item.scanTime);
                            tr += "</td>";

                            tr += "</tr>";

                            tbody.append(tr)
                        });
                    }
                } else {
                    tbody.empty();
                    tbody.append("<tr><td  style='text-align: center' colspan='6'>没有任何扫码记录！</td></tr>")
                }
            })
    }
});