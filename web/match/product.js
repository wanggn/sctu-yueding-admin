var matches = $("#matches");
var routes = $("#routes");
var tbody = $("#product_list");
var pageIndex = 1;
var pageSize = 10;
var pageTotal = 0;

var pageIndexWithMatchIdAndRouteId = 1;
var pageSizeWithMatchIdAndRouteId = 10;
var pageTotalWithMatchIdAndRouteId = 0;

$(function () {
    AjaxGetRoutes(pageIndex, pageSize);
    $.getJSON(baseUrl + "/matches", function (json) {
        var data = json.data;
        matches.empty();
        routes.empty();
        matches.append('<option value="all">所有</option>');
        routes.append('<option value="all">所有</option>');
        $.each(data, function (i, match) {
            var option = "";
            option += "<option value='" + match.id + "'>";
            option += match.name;
            option += "</option>";
            matches.append(option);
        })
    });
});

matches.change(function () {
    var matchId = $("#matches option:selected").val();
    if (matchId == "all") {
        window.location.reload();
    } else {
        $.getJSON(baseUrl + "/matches/" + matchId + "/routes",
            function (json) {
                var data = json.data;
                routes.empty();
                $.each(data, function (i, route) {
                    var option = "";
                    if (i == 0) {
                        option += "<option value='" + route.id + "' selected = 'selected'>";
                        option += route.introduction;
                        option += "</option>";
                        routes.append(option);


                    } else {
                        option += "<option value='" + route.id + "'>";
                        option += route.introduction;
                        option += "</option>";
                        routes.append(option);
                    }
                })
            })
    }
});

routes.change(function () {
    var matchId = $("#matches option:selected").val();
    var routeId = $("#routes option:selected").val();

    AjaxGetDataWithMatchIdAndRouteId(matchId, routeId, pageIndexWithMatchIdAndRouteId, pageSizeWithMatchIdAndRouteId);

    $('.first').attr('onclick', 'GoToFirstPageWithMatchIdAndRouteId(' + matchId + ',"' + routeId + '")');
    $('.prev').attr('onclick', 'GoToPrePageWithMatchIdAndRouteId(' + matchId + ',"' + routeId + '")');
    $('.next').attr('onclick', 'GoToNextPageWithMatchIdAndRouteId(' + matchId + ',"' + routeId + '")');
    $('.run').attr('onclick', 'GoToAppointPageWithMatchIdAndRouteId(' + matchId + ',"' + routeId + '")');
    $('.last').attr('onclick', 'GoToEndPageWithMatchIdAndRouteId(' + matchId + ',"' + routeId + '")');

});


function AjaxGetRoutes(index, size) {
    $.ajax({
        url: baseUrl + "/entryProducts",
        type: "GET",
        data: {page: index - 1, size: size},
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotal = Math.ceil(recordCount / size);
            $(".currentPage").text("第" + index + "页");
            $(".total").text("共" + pageTotal + "页");
            var data = json.data;
            tbody.empty();
            $.each(data, function (i, item) {
                var tr = "";
                tr += "<tr id='" + item.id + "'>";

                tr += "<td>";
                tr += item.department;
                tr += "</td>";

                tr += "<td>";
                tr += item.professional;
                tr += "</td>";

                tr += "<td>";
                tr += item.grade;
                tr += "</td>";

                tr += "<td>";
                tr += item.teamName;
                tr += "</td>";

                tr += "<td>";
                tr += item.productName;
                tr += "</td>";

                tr += "<td>";
                tr += format(item.createDate);
                tr += "</td>";

                tr += "<td>";
                tr += '<a href="' + item.productUrl + '">下载作品</a>';
                tr += "</td>";

                tr += "</tr>";

                tbody.append(tr)
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPage() {
    pageIndex = 1;
    AjaxGetRoutes(pageIndex, pageSize);
}

function GoToPrePage() {
    pageIndex -= 1;
    pageIndex = pageIndex >= 1 ? pageIndex : 1;
    AjaxGetRoutes(pageIndex, pageSize);
}

function GoToNextPage() {
    if (pageIndex < pageTotal) {
        pageIndex += 1;
    }
    AjaxGetRoutes(pageIndex, pageSize);
}

function GoToEndPage() {
    pageIndex = pageTotal;
    AjaxGetRoutes(pageIndex, pageSize);
}

function GoToAppointPage() {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndex = pageIndex;
        pageIndex = parseInt($(".num>input").val());
        if (pageIndex <= 0 || pageIndex > pageTotal) {
            pageIndex = tempPageIndex;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetRoutes(pageIndex, pageSize);
        }
    }
}


function AjaxGetDataWithMatchIdAndRouteId(matchId, routeId, index, size) {
    $.ajax({
        url: baseUrl + "/matches/" + matchId + "/routes/" + routeId + "/entryProducts",
        type: "GET",
        data: {page: index - 1, size: size},
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotal = Math.ceil(recordCount / size);
            $(".currentPage").text("第" + index + "页");
            $(".total").text("共" + pageTotal + "页");
            var data = json.data;
            tbody.empty();
            if (data.length == 0) {
                var tr = "";

                tr += "<tr>";

                tr += "<td style='text-align: center' colspan='7'>";
                tr += "该路线下没有相关作品";
                tr += "</td>";

                tr += "</tr>";
                tbody.append(tr)
            } else {

                $.each(data, function (i, item) {
                    var tr = "";
                    tr += "<tr id='" + item.id + "'>";

                    tr += "<td>";
                    tr += item.department;
                    tr += "</td>";

                    tr += "<td>";
                    tr += item.professional;
                    tr += "</td>";

                    tr += "<td>";
                    tr += item.grade;
                    tr += "</td>";

                    tr += "<td>";
                    tr += item.teamName;
                    tr += "</td>";

                    tr += "<td>";
                    tr += item.productName;
                    tr += "</td>";

                    tr += "<td>";
                    tr += format(item.createDate);
                    tr += "</td>";

                    tr += "<td>";
                    tr += '<a href="' + item.productUrl + '">下载作品</a>';
                    tr += "</td>";

                    tr += "</tr>";

                    tbody.append(tr)
                });

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPageWithMatchIdAndRouteId(matchId, routeId) {
    pageIndexWithMatchIdAndRouteId = 1;
    AjaxGetDataWithMatchIdAndRouteId(matchId, routeId, pageIndexWithMatchIdAndRouteId, pageSizeWithMatchIdAndRouteId)
}

function GoToPrePageWithMatchIdAndRouteId(matchId, routeId) {
    pageIndexWithMatchIdAndRouteId -= 1;
    pageIndexWithMatchIdAndRouteId = pageIndexWithMatchIdAndRouteId >= 1 ? pageIndexWithMatchIdAndRouteId : 1;
    AjaxGetDataWithMatchIdAndRouteId(matchId, routeId, pageIndexWithMatchIdAndRouteId, pageSizeWithMatchIdAndRouteId)
}

function GoToNextPageWithMatchIdAndRouteId(matchId, routeId) {
    if (pageIndexWithMatchIdAndRouteId < pageTotalWithMatchIdAndRouteId) {
        pageIndexWithMatchIdAndRouteId += 1;
    }
    AjaxGetDataWithMatchIdAndRouteId(matchId, routeId, pageIndexWithMatchIdAndRouteId, pageSizeWithMatchIdAndRouteId)
}

function GoToEndPageWithMatchIdAndRouteId(matchId, routeId) {
    pageIndexWithMatchIdAndRouteId = pageTotalWithMatchIdAndRouteId;
    AjaxGetDataWithMatchIdAndRouteId(matchId, routeId, pageIndexWithMatchIdAndRouteId, pageSizeWithMatchIdAndRouteId)
}

function GoToAppointPageWithMatchIdAndRouteId(matchId, routeId) {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndexWithMatchIdAndRouteId = pageIndexWithMatchIdAndRouteId;
        pageIndexWithMatchIdAndRouteId = parseInt($(".num>input").val());
        if (pageIndexWithMatchIdAndRouteId <= 0 || pageIndexWithMatchIdAndRouteId > pageTotalWithMatchIdAndRouteId) {
            pageIndexWithMatchIdAndRouteId = tempPageIndexWithMatchIdAndRouteId;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetDataWithMatchIdAndRouteId(matchId, routeId, pageIndexWithMatchIdAndRouteId, pageSizeWithMatchIdAndRouteId)
        }
    }
}

function exportProductExcel() {
    var matchId = $("#matches option:selected").val();
    if (matchId == "all") {
        alert("请选择一个比赛")
    } else {
        $.getJSON(baseUrl + "/matches/" + matchId + "/entryProducts/export",function(json){
            if(json.meta.code == 200){
                $.get(json.data);
            }
        })
    }
}

