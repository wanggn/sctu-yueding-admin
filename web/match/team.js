var pageIndex = 1;
var pageSize = 10;
var pageTotal = 0;
var tbody = $("#team_list");
var matches = $("#matches");
var routes = $("#routes");
$(function () {
    pageIndex = 1;
    AjaxGetTeams(pageIndex, pageSize);
    $.getJSON(baseUrl + "/matches", function (json) {
        var data = json.data;
        matches.empty();
        matches.append('<option value="all">所有</option>');
        routes.append('<option value="all">所有</option>');
        $.each(data, function (i, item) {
            matches.append("<option value='" + item.id + "'>" + item.name + "</option>")
        })
    })
});

function AjaxGetTeams(index, size) {
    $.ajax({
        url: baseUrl + "/teams",
        type: "GET",
        data: {page: index - 1, size: size},
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotal = Math.ceil(recordCount / size);
            $(".currentPage").text("第" + index + "页");
            $(".total").text("共" + pageTotal + "页");
            var data = json.data;
            tbody.empty();
            $.each(data, function (i, item) {
                var tr = "";
                tr += "<tr>";

                tr += "<td>";
                tr += item.match;
                tr += "</td>";

                tr += "<td>";
                tr += item.route;
                tr += "</td>";

                tr += "<td>";
                tr += item.routeType;
                tr += "</td>";

                tr += "<td>";
                tr += item.team;
                tr += "</td>";

                tr += "<td>";
                tr += item.members;
                tr += "</td>";

                tr += "<td>";
                tr += '<a href="javascript:edit(\'../T/T_editTeam.html?teamId=' + item.id + '\',\'成员管理\')">成员管理</a>';
                tr += "</td>";

                tr += "</tr>";

                tbody.append(tr)
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPage() {
    pageIndex = 1;
    AjaxGetTeams(pageIndex, pageSize);
}

function GoToPrePage() {
    pageIndex -= 1;
    pageIndex = pageIndex >= 1 ? pageIndex : 1;
    AjaxGetTeams(pageIndex, pageSize);
}

function GoToNextPage() {
    if (pageIndex < pageTotal) {
        pageIndex += 1;
    }
    AjaxGetTeams(pageIndex, pageSize);
}

function GoToEndPage() {
    pageIndex = pageTotal;
    AjaxGetTeams(pageIndex, pageSize);
}

function GoToAppointPage() {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndex = pageIndex;
        pageIndex = parseInt($(".num>input").val());
        if (pageIndex <= 0 || pageIndex > pageTotal) {
            pageIndex = tempPageIndex;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetTeams(pageIndex, pageSize);
        }
    }
}

var pageIndexWithMatchId = 1;
var pageSizeWithMatchId = 10;
var pageTotalWithMatchId = 0;
matches.change(function () {
    var matchId = $("#matches option:selected").val();
    $('#export').attr('href', baseUrl + "/matches/" + matchId + "/teams/export");
    if (matchId == "all") {
        window.location.reload();
    } else {
        AjaxGetTeamsWithMatchId(matchId, pageIndexWithMatchId, pageSizeWithMatchId);
        $('.first').attr('onclick', 'GoToFirstPageWithMatchId(' + matchId + ')');
        $('.prev').attr('onclick', 'GoToPrePageWithMatchId(' + matchId + ')');
        $('.next').attr('onclick', 'GoToNextPageWithMatchId(' + matchId + ')');
        $('.run').attr('onclick', 'GoToAppointPageWithMatchId(' + matchId + ')');
        $('.last').attr('onclick', 'GoToEndPageWithMatchId(' + matchId + ')');

        $.getJSON(baseUrl + "/matches/" + matchId + "/routeTypes", function (json) {
            var data = json.data;
            var routes = $("#routes");
            routes.empty();
            $.each(data, function (i, item) {
                var option = "";
                option += "<option value='" + item + "'>";
                option += item;
                option += "</option>";
                routes.append(option);
            })
        })
    }
});

function AjaxGetTeamsWithMatchId(matchId, index, size) {
    $.ajax({
        url: baseUrl + "/matches/" + matchId + "/teams",
        type: "GET",
        data: {matchId: matchId, page: index - 1, size: size},
        dataType: "json",
        success: function (json) {
            var recordCountWithMatchId = json.pagination.nextMinId;
            pageTotalWithMatchId = Math.ceil(recordCountWithMatchId / size);
            $(".currentPage").text("第" + index + "页");
            $(".total").text("共" + pageTotalWithMatchId + "页");
            var data = json.data;
            tbody.empty();
            $.each(data, function (i, item) {
                var tr = "";
                tr += "<tr>";

                tr += "<td>";
                tr += item.match;
                tr += "</td>";

                tr += "<td>";
                tr += item.route;
                tr += "</td>";

                tr += "<td>";
                tr += item.routeType;
                tr += "</td>";

                tr += "<td>";
                tr += item.team;
                tr += "</td>";

                tr += "<td>";
                tr += item.members;
                tr += "</td>";

                tr += "<td>";
                tr += '<a href="javascript:edit(\'../T/T_editTeam.html?teamId=' + item.id + '\',\'成员管理\')">成员管理</a>';
                tr += "</td>";

                tr += "</tr>";

                tbody.append(tr)
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPageWithMatchId(matchId) {
    pageIndexWithMatchId = 1;
    AjaxGetTeamsWithMatchId(matchId, pageIndexWithMatchId, pageSizeWithMatchId)
}

function GoToPrePageWithMatchId(matchId) {
    pageIndexWithMatchId -= 1;
    pageIndexWithMatchId = pageIndexWithMatchId >= 1 ? pageIndexWithMatchId : 1;
    AjaxGetTeamsWithMatchId(matchId, pageIndexWithMatchId, pageSizeWithMatchId)
}

function GoToNextPageWithMatchId(matchId) {
    if (pageIndexWithMatchId < pageTotalWithMatchId) {
        pageIndexWithMatchId += 1;
    }
    AjaxGetTeamsWithMatchId(matchId, pageIndexWithMatchId, pageSizeWithMatchId)
}

function GoToEndPageWithMatchId(matchId) {
    pageIndexWithMatchId = pageTotalWithMatchId;
    AjaxGetTeamsWithMatchId(matchId, pageIndexWithMatchId, pageSizeWithMatchId)
}

function GoToAppointPageWithMatchId(matchId) {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndexWithMatchId = pageIndexWithMatchId;
        pageIndexWithMatchId = parseInt($(".num>input").val());
        if (pageIndexWithMatchId <= 0 || pageIndexWithMatchId > pageTotalWithMatchId) {
            pageIndexWithMatchId = tempPageIndexWithMatchId;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetTeamsWithMatchId(matchId, pageIndexWithMatchId, pageSizeWithMatchId)
        }
    }
}


var pageIndexWithMatchIdAndRouteId = 1;
var pageSizeWithMatchIdAndRouteId = 10;
var pageTotalWithMatchIdAndRouteId = 0;
routes.change(function () {
    var matchId = $("#matches option:selected").val();
    var routeType = $("#routes option:selected").val();
    AjaxGetTeamsWithMatchIdAndRouteId(matchId, routeType, pageIndexWithMatchIdAndRouteId, pageSizeWithMatchIdAndRouteId);
    $('.first').attr('onclick', 'GoToFirstPageWithMatchIdAndRouteId(' + matchId + ',"' + routeType + '")');
    $('.prev').attr('onclick', 'GoToPrePageWithMatchIdAndRouteId(' + matchId + ',"' + routeType + '")');
    $('.next').attr('onclick', 'GoToNextPageWithMatchIdAndRouteId(' + matchId + ',"' + routeType + '")');
    $('.run').attr('onclick', 'GoToAppointPageWithMatchIdAndRouteId(' + matchId + ',"' + routeType + '")');
    $('.last').attr('onclick', 'GoToEndPageWithMatchIdAndRouteId(' + matchId + ',"' + routeType + '")');
});

function AjaxGetTeamsWithMatchIdAndRouteId(matchId, routeType, index, size) {
    $.ajax({
        url: baseUrl + "/matches/" + matchId + "/routes/" + routeType + "/teams",
        type: "GET",
        data: {page: index - 1, size: size},
        dataType: "json",
        success: function (json) {
            var recordCountWithMatchIdAndRouteId = json.pagination.nextMinId;
            pageTotalWithMatchIdAndRouteId = Math.ceil(recordCountWithMatchIdAndRouteId / size);
            $(".currentPage").text("第" + index + "页");
            $(".total").text("共" + pageTotalWithMatchIdAndRouteId + "页");
            var data = json.data;
            tbody.empty();
            $.each(data, function (i, item) {
                var tr = "";
                tr += "<tr>";

                tr += "<td>";
                tr += item.match;
                tr += "</td>";

                tr += "<td>";
                tr += item.route;
                tr += "</td>";

                tr += "<td>";
                tr += item.routeType;
                tr += "</td>";

                tr += "<td>";
                tr += item.team;
                tr += "</td>";

                tr += "<td>";
                tr += item.members;
                tr += "</td>";

                tr += "<td>";
                tr += '<a href="javascript:edit(\'../T/T_editTeam.html?teamId=' + item.id + '\',\'成员管理\')">成员管理</a>';
                tr += "</td>";

                tr += "</tr>";

                tbody.append(tr)
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPageWithMatchIdAndRouteId(matchId, routeType) {
    pageIndexWithMatchIdAndRouteId = 1;
    AjaxGetTeamsWithMatchIdAndRouteId(matchId, routeType, pageIndexWithMatchIdAndRouteId, pageSizeWithMatchIdAndRouteId)
}

function GoToPrePageWithMatchIdAndRouteId(matchId, routeType) {
    pageIndexWithMatchIdAndRouteId -= 1;
    pageIndexWithMatchIdAndRouteId = pageIndexWithMatchIdAndRouteId >= 1 ? pageIndexWithMatchIdAndRouteId : 1;
    AjaxGetTeamsWithMatchIdAndRouteId(matchId, routeType, pageIndexWithMatchIdAndRouteId, pageSizeWithMatchIdAndRouteId)
}

function GoToNextPageWithMatchIdAndRouteId(matchId, routeType) {
    if (pageIndexWithMatchIdAndRouteId < pageTotalWithMatchIdAndRouteId) {
        pageIndexWithMatchIdAndRouteId += 1;
    }
    AjaxGetTeamsWithMatchIdAndRouteId(matchId, routeType, pageIndexWithMatchIdAndRouteId, pageSizeWithMatchIdAndRouteId)
}

function GoToEndPageWithMatchIdAndRouteId(matchId, routeType) {
    pageIndexWithMatchIdAndRouteId = pageTotalWithMatchIdAndRouteId;
    AjaxGetTeamsWithMatchIdAndRouteId(matchId, routeType, pageIndexWithMatchIdAndRouteId, pageSizeWithMatchIdAndRouteId)
}

function GoToAppointPageWithMatchIdAndRouteId(matchId, routeType) {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndexWithMatchIdAndRouteId = pageIndexWithMatchIdAndRouteId;
        pageIndexWithMatchIdAndRouteId = parseInt($(".num>input").val());
        if (pageIndexWithMatchIdAndRouteId <= 0 || pageIndexWithMatchIdAndRouteId > pageTotalWithMatchIdAndRouteId) {
            pageIndexWithMatchIdAndRouteId = tempPageIndexWithMatchIdAndRouteId;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetTeamsWithMatchIdAndRouteId(matchId, routeType, pageIndexWithMatchIdAndRouteId, pageSizeWithMatchIdAndRouteId)
        }
    }
}

function exportExcel() {
    var matchId = $("#matches option:selected").val();
    if (matchId == "all") {
        alert("请选择一个比赛")
    } else {
        $.get(baseUrl + "/matches/" + matchId + "/teams/export")
    }
    //$("#export").attr("href", baseUrl + "/matches/" + matchId + "/teams/export")
}
