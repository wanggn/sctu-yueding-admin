var pageIndex = 1;
var pageSize = 10;
var pageTotal = 0;
var pageIndexWithCondition = 1;
var pageSizeWithCondition = 10;
var pageTotalWithCondition = 0;
var tbody = $("#match_list");
$(function () {
    pageIndex = 1;
    AjaxGetMatches(pageIndex, pageSize);
});
function AjaxGetMatches(index, size) {
    $.ajax({
        url: baseUrl + "/matches",
        type: "GET",
        data: {page: index - 1, size: size},
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotal = Math.ceil(recordCount / size);
            $(".currentPage").text("第" + index + "页");
            $(".total").text("共" + pageTotal + "页");
            var data = json.data;
            tbody.empty();
            $.each(data, function (i, match) {
                var tr = "";

                tr += "<tr id='"+match.id+"'>";

                tr += "<td>";
                tr += match.name;
                tr += "</td>";

                tr += "<td>";
                tr += match.matchPlace;
                tr += "</td>";

                tr += "<td>";
                tr += match.sponsor;
                tr += "</td>";

                tr += "<td>";
                tr += match.phoneNumber;
                tr += "</td>";

                tr += "<td>";
                tr += match.enrollStart;
                tr += "</td>";

                tr += "<td>";
                tr += match.enrollEnd;
                tr += "</td>";

                tr += "<td>";
                tr += match.matchStart;
                tr += "</td>";

                tr += "<td>";
                tr += match.matchEnd;
                tr += "</td>";

                tr += "<td>";
                tr += '<a href="javascript:details(\'../T/T_MatchDetails.html?matchId=' + match.id + '\',\'比赛详情\',[\'710px\', \'500px\']);">详情</a>';
                tr += "</td>";

                tr += "<td>";
                tr += '<a href="javascript:edit(\'../T/T_editMatch.html?matchId=' + match.id + '\',\'新闻内容修改\')">修改</a> | <a href="javascript:confirmurl(' + match.id + ',\'确认要删除吗？\')">删除</a>';
                tr += "</td>";

                tr += "</tr>";
                tbody.append(tr);
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败");
        }
    });
}

function GoToFirstPage() {
    pageIndex = 1;
    AjaxGetMatches(pageIndex, pageSize);
}

function GoToPrePage() {
    pageIndex -= 1;
    pageIndex = pageIndex >= 1 ? pageIndex : 1;
    AjaxGetMatches(pageIndex, pageSize);
}

function GoToNextPage() {
    if (pageIndex < pageTotal) {
        pageIndex += 1;
    }
    AjaxGetMatches(pageIndex, pageSize);
}

function GoToEndPage() {
    pageIndex = pageTotal;
    AjaxGetMatches(pageIndex, pageSize);
}

function GoToAppointPage() {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndex = pageIndex;
        pageIndex = parseInt($(".num>input").val());
        if (pageIndex <= 0 || pageIndex > pageTotal) {
            pageIndex = tempPageIndex;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetMatches(pageIndex, pageSize);
        }
    }
}

$("#isEnd").change(function () {
    var isEnd = $("#isEnd option:selected").val();
    if (isEnd == "all") {
        AjaxGetMatches(pageIndex,pageSize);
    } else {
        AjaxGetMatchesByCondition(isEnd, pageIndexWithCondition, pageSizeWithCondition);
        $('.first').attr('onclick','GoToFirstPageWithCondition('+isEnd+')');
        $('.prev').attr('onclick','GoToPrePageWithCondition('+isEnd+')');
        $('.next').attr('onclick','GoToNextPageWithCondition('+isEnd+')');
        $('.run').attr('onclick','GoToAppointPageWithCondition('+isEnd+')');
        $('.last').attr('onclick','GoToEndPageWithCondition('+isEnd+')');
    }
});

function AjaxGetMatchesByCondition(isEnd, index, size) {
    $.ajax({
        url: baseUrl + "/matches/classify",
        type: "GET",
        data: {isEnd : isEnd, page: index - 1, size: size},
        dataType: "json",
        success: function (json) {
            var recordCountWithCondition = json.pagination.nextMinId;
            pageTotalWithCondition = Math.ceil(recordCountWithCondition / size);
            $(".currentPage").text("第" + index + "页");
            $(".total").text("共" + pageTotalWithCondition + "页");
            var data = json.data;
            tbody.empty();
            $.each(data, function (i, match) {
                var tr = "";

                tr += "<tr id='"+match.id+"'>";

                tr += "<td>";
                tr += match.name;
                tr += "</td>";

                tr += "<td>";
                tr += match.matchPlace;
                tr += "</td>";

                tr += "<td>";
                tr += match.sponsor;
                tr += "</td>";

                tr += "<td>";
                tr += match.phoneNumber;
                tr += "</td>";

                tr += "<td>";
                tr += match.enrollStart;
                tr += "</td>";

                tr += "<td>";
                tr += match.enrollEnd;
                tr += "</td>";

                tr += "<td>";
                tr += match.matchStart;
                tr += "</td>";

                tr += "<td>";
                tr += match.matchEnd;
                tr += "</td>";

                tr += "<td>";
                tr += '<a href="javascript:details(\'../T/T_MatchDetails.html?matchId=' + match.id + '\',\'比赛详情\',[\'710px\', \'500px\']);">详情</a>';
                tr += "</td>";

                tr += "<td>";
                tr += '<a href="javascript:edit(\'../T/T_editMatch.html?matchId=' + match.id + '\',\'新闻内容修改\')">修改</a> | <a href="javascript:confirmurl(' + match.id + ',\'确认要删除吗？\')">删除</a>';
                tr += "</td>";

                tr += "</tr>";
                tbody.append(tr);
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");

        }
    });
}

function GoToFirstPageWithCondition(isEnd) {
    pageIndexWithCondition = 1;
    AjaxGetMatchesByCondition(isEnd, pageIndexWithCondition, pageSizeWithCondition)
}

function GoToPrePageWithCondition(isEnd) {
    pageIndexWithCondition -= 1;
    pageIndexWithCondition = pageIndexWithCondition >= 1 ? pageIndexWithCondition : 1;
    AjaxGetMatchesByCondition(isEnd, pageIndexWithCondition, pageSizeWithCondition)
}

function GoToNextPageWithCondition(isEnd) {
    if (pageIndexWithCondition < pageTotalWithCondition) {
        pageIndexWithCondition += 1;
    }
    AjaxGetMatchesByCondition(isEnd, pageIndexWithCondition, pageSizeWithCondition)
}

function GoToEndPageWithCondition(isEnd) {
    pageIndexWithCondition = pageTotalWithCondition;
    AjaxGetMatchesByCondition(isEnd, pageIndexWithCondition, pageSizeWithCondition)
}

function GoToAppointPageWithCondition(isEnd) {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndexWithCondition = pageIndexWithCondition;
        pageIndexWithCondition = parseInt($(".num>input").val());
        if (pageIndexWithCondition <= 0 || pageIndexWithCondition > pageTotalWithCondition) {
            pageIndexWithCondition = tempPageIndexWithCondition;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetMatchesByCondition(isEnd, pageIndexWithCondition, pageSizeWithCondition)
        }
    }
}

