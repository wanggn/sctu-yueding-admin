var pageIndex = 1;
var pageSize = 10;
var pageTotal = 0;
var tbody = $("#order_list");
$(function () {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
});

function AjaxGetData(index, size) {
    $.ajax({
        url: baseUrl + "/orders",
        type: "GET",
        data: {page: index - 1, size: size},
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotal = Math.ceil(recordCount / pageSize);
            $(".first").text("第" + index + "页");
            $(".total").text("共" + pageTotal + "页");
            var data = json.data;
            tbody.empty();

            $.each(data, function (i, order) {
                var tr = "";

                tr += "<tr>";

                tr += "<td>";
                tr += order.orderNo;
                tr += "</td>";

                tr += "<td>";
                tr += order.creator;
                tr += "</td>";

                tr += "<td>";
                tr += order.createTime;
                tr += "</td>";

                tr += "<td>";
                tr += order.totalAmount;
                tr += "</td>";

                if (order.status == 0) {
                    tr += "<td>";
                    tr += '<span>已支付</span>';
                    tr += "</td>";
                } else if (order.status == 1) {
                    tr += "<td>";
                    tr += '<span>正在支付</span>';
                    tr += "</td>";
                } else if (order.status == 3) {
                    tr += "<td>";
                    tr += '<span>未支付</span>';
                    tr += "</td>";
                }

                if (order.payChannel == "wxpay") {
                    tr += "<td>";
                    tr += '<span>微信</span>';
                    tr += "</td>";
                } else if (order.payChannel == "alipay") {
                    tr += "<td>";
                    tr += '<span>支付宝</span>';
                    tr += "</td>";
                } else {
                    tr += "<td>";
                    tr += '<span>未知</span>';
                    tr += "</td>";
                }


                tbody.append(tr);
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPage() {
    pageIndex = 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToPrePage() {
    pageIndex -= 1;
    pageIndex = pageIndex >= 1 ? pageIndex : 1;
    AjaxGetData(pageIndex, pageSize);
}

function GoToNextPage() {
    if (pageIndex < pageTotal) {
        pageIndex += 1;
    }
    AjaxGetData(pageIndex, pageSize);
}

function GoToEndPage() {
    pageIndex = pageTotal;
    AjaxGetData(pageIndex, pageSize);
}

function GoToAppointPage() {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndex = pageIndex;
        pageIndex = parseInt($(".num>input").val());
        if (pageIndex <= 0 || pageIndex > pageTotal) {
            pageIndex = tempPageIndex;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetData(pageIndex, pageSize);
        }
    }
}


//************************根据是否支付筛选查找************************
var pageIndexWithIsPay = 1;
var pageSizeWithIsPay = 10;
var pageTotalWithIsPay = 0;

$("#isPay").change(function () {
    var isPay = $("#isPay option:selected").val();
    if (isPay == "all") {
        pageIndex = 1;
        AjaxGetData(pageIndex, pageSize);
    } else {
        AjaxGetDataByIsPay(isPay, pageIndexWithIsPay, pageSizeWithIsPay);
        $('.first').attr('onclick', 'GoToFirstPageWithIsPay(' + isPay + ')');
        $('.prev').attr('onclick', 'GoToPrePageWithIsPay(' + isPay + ')');
        $('.next').attr('onclick', 'GoToNextPageWithIsPay(' + isPay + ')');
        $('.run').attr('onclick', 'GoToAppointPageWithIsPay(' + isPay + ')');
        $('.last').attr('onclick', 'GoToEndPageWithIsPay(' + isPay + ')');
    }
});

function AjaxGetDataByIsPay(isPay, pageIndexWithIsPay, pageSizeWithIsPay) {
    $.ajax({
        url: baseUrl + "/orders/classify",
        type: "GET",
        data: {
            conditionType: "isPay",
            value: isPay,
            page: pageIndexWithIsPay - 1,
            size: pageSizeWithIsPay
        },
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotalWithIsPay = Math.ceil(recordCount / pageSizeWithIsPay);
            $(".first").text("第" + pageIndexWithIsPay + "页");
            $(".total").text("共" + pageTotalWithIsPay + "页");
            var data = json.data;
            tbody.empty();

            $.each(data, function (i, order) {
                var tr = "";

                tr += "<tr>";

                tr += "<td>";
                tr += order.orderNo;
                tr += "</td>";

                tr += "<td>";
                tr += order.creator;
                tr += "</td>";

                tr += "<td>";
                tr += order.createTime;
                tr += "</td>";

                tr += "<td>";
                tr += order.totalAmount;
                tr += "</td>";

                if (order.status == 0) {
                    tr += "<td>";
                    tr += '<span>已支付</span>';
                    tr += "</td>";
                } else if (order.status == 1) {
                    tr += "<td>";
                    tr += '<span>正在支付</span>';
                    tr += "</td>";
                } else if (order.status == 3) {
                    tr += "<td>";
                    tr += '<span>未支付</span>';
                    tr += "</td>";
                }

                if (order.payChannel == "wxpay") {
                    tr += "<td>";
                    tr += '<span>微信</span>';
                    tr += "</td>";
                } else if (order.payChannel == "alipay") {
                    tr += "<td>";
                    tr += '<span>支付宝</span>';
                    tr += "</td>";
                } else {
                    tr += "<td>";
                    tr += '<span>未知</span>';
                    tr += "</td>";
                }
                tbody.append(tr);
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPageWithIsPay(isPay) {
    pageIndexWithIsPay = 1;
    AjaxGetDataByIsPay(isPay, pageIndexWithIsPay, pageSizeWithIsPay)
}

function GoToPrePageWithIsPay(isPay) {
    pageIndexWithIsPay -= 1;
    pageIndexWithIsPay = pageIndexWithIsPay >= 1 ? pageIndexWithIsPay : 1;
    AjaxGetDataByIsPay(isPay, pageIndexWithIsPay, pageSizeWithIsPay)
}

function GoToNextPageWithIsPay(isPay) {
    if (pageIndexWithIsPay < pageTotalWithIsPay) {
        pageIndexWithIsPay += 1;
    }
    AjaxGetDataByIsPay(isPay, pageIndexWithIsPay, pageSizeWithIsPay)
}

function GoToEndPageWithIsPay(isPay) {
    pageIndexWithIsPay = pageTotalWithIsPay;
    AjaxGetDataByIsPay(isPay, pageIndexWithIsPay, pageSizeWithIsPay)
}

function GoToAppointPageWithIsPay(isPay) {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndexWithIsPay = pageIndexWithIsPay;
        pageIndexWithIsPay = parseInt($(".num>input").val());
        if (pageIndexWithIsPay <= 0 || pageIndexWithIsPay > pageTotalWithIsPay) {
            pageIndexWithIsPay = tempPageIndexWithIsPay;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetDataByIsPay(isPay, pageIndexWithIsPay, pageSizeWithIsPay)
        }
    }
}


//************************根据支付方式筛选查找************************

var pageIndexWithPayChannel = 1;
var pageSizeWithPayChannel = 10;
var pageTotalWithPayChannel = 0;

$("#payChannel").change(function () {
    var payChannel = $("#payChannel option:selected").val();
    if (payChannel == "all") {
        AjaxGetData(1, pageSize);
    } else {
        AjaxGetDataByPayChannel(payChannel, pageIndexWithPayChannel, pageSizeWithPayChannel);
        $('.first').attr('onclick', 'GoToFirstPageWithPayChannel("' + payChannel + '")');
        $('.prev').attr('onclick', 'GoToPrePageWithPayChannel("' + payChannel + '")');
        $('.next').attr('onclick', 'GoToNextPageWithPayChannel("' + payChannel + '")');
        $('.run').attr('onclick', 'GoToAppointPageWithPayChannel("' + payChannel + '")');
        $('.last').attr('onclick', 'GoToEndPageWithPayChannel("' + payChannel + '")');
    }
});

function AjaxGetDataByPayChannel(payChannel, pageIndexWithPayChannel, pageSizeWithPayChannel) {
    $.ajax({
        url: baseUrl + "/orders/classify",
        type: "GET",
        data: {
            conditionType: "payChannel",
            value: payChannel,
            page: pageIndexWithPayChannel - 1,
            size: pageSizeWithPayChannel
        },
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotalWithPayChannel = Math.ceil(recordCount / pageSizeWithPayChannel);
            $(".first").text("第" + pageIndexWithPayChannel + "页");
            $(".total").text("共" + pageTotalWithPayChannel + "页");
            var data = json.data;
            tbody.empty();

            $.each(data, function (i, order) {
                var tr = "";

                tr += "<tr>";

                tr += "<td>";
                tr += order.orderNo;
                tr += "</td>";

                tr += "<td>";
                tr += order.creator;
                tr += "</td>";

                tr += "<td>";
                tr += order.createTime;
                tr += "</td>";

                tr += "<td>";
                tr += order.totalAmount;
                tr += "</td>";

                if (order.status == 0) {
                    tr += "<td>";
                    tr += '<span>已支付</span>';
                    tr += "</td>";
                } else if (order.status == 1) {
                    tr += "<td>";
                    tr += '<span>正在支付</span>';
                    tr += "</td>";
                } else if (order.status == 3) {
                    tr += "<td>";
                    tr += '<span>未支付</span>';
                    tr += "</td>";
                }

                if (order.payChannel == "wxpay") {
                    tr += "<td>";
                    tr += '<span>微信</span>';
                    tr += "</td>";
                } else if (order.payChannel == "alipay") {
                    tr += "<td>";
                    tr += '<span>支付宝</span>';
                    tr += "</td>";
                } else {
                    tr += "<td>";
                    tr += '<span>未知</span>';
                    tr += "</td>";
                }
                tbody.append(tr);
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败!");
        }
    });
}

function GoToFirstPageWithPayChannel(payChannel) {
    pageIndexWithPayChannel = 1;
    AjaxGetDataByPayChannel(payChannel, pageIndexWithPayChannel, pageSizeWithPayChannel)
}

function GoToPrePageWithPayChannel(payChannel) {
    pageIndexWithPayChannel -= 1;
    pageIndexWithPayChannel = pageIndexWithPayChannel >= 1 ? pageIndexWithPayChannel : 1;
    AjaxGetDataByPayChannel(payChannel, pageIndexWithPayChannel, pageSizeWithPayChannel)
}

function GoToNextPageWithPayChannel(payChannel) {
    if (pageIndexWithPayChannel < pageTotalWithPayChannel) {
        pageIndexWithPayChannel += 1;
    }
    AjaxGetDataByPayChannel(payChannel, pageIndexWithPayChannel, pageSizeWithPayChannel)
}

function GoToEndPageWithPayChannel(payChannel) {
    pageIndexWithPayChannel = pageTotalWithPayChannel;
    AjaxGetDataByPayChannel(payChannel, pageIndexWithPayChannel, pageSizeWithPayChannel)
}

function GoToAppointPageWithPayChannel(payChannel) {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndexWithPayChannel = pageIndexWithPayChannel;
        pageIndexWithPayChannel = parseInt($(".num>input").val());
        if (pageIndexWithPayChannel <= 0 || pageIndexWithPayChannel > pageTotalWithPayChannel) {
            pageIndexWithPayChannel = tempPageIndexWithPayChannel;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetDataByPayChannel(payChannel, pageIndexWithPayChannel, pageSizeWithPayChannel)
        }
    }
}
