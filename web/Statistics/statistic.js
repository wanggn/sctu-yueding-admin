var matches = $("#matches");

$(function () {
    init();
    $.getJSON(baseUrl + "/matches", function (json) {
        var data = json.data;
        matches.empty();
        matches.append('<option value="all">不限</option>');
        $.each(data, function (i, item) {
            matches.append("<option value='" + item.id + "'>" + item.name + "</option>")
        })
    })
});

function init() {
    $.getJSON(baseUrl + "/orderStatistics", function (json) {
        if (json.meta.code == 200) {
            var data = json.data;

            var myChart = echarts.init(document.getElementById('pay'));
            var option = {
                title: {
                    text: '支付情况',
                    subtext: '支付状态',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: ['已支付', '未支付', '正在支付中']
                },
                series: [
                    {
                        name: '',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: [
                            {value: data.isPay, name: '已支付'},
                            {value: data.isNotPay, name: '未支付'},
                            {value: data.isPaying, name: '正在支付中'}
                        ],
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            myChart.setOption(option);

            var chart = echarts.init(document.getElementById('money'));
            var payChannelOption = {
                title: {
                    text: '',
                    subtext: '支付渠道',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: ['微信支付', '支付宝支付']
                },
                series: [
                    {
                        name: '',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%'],
                        data: [
                            {value: data.wxpay, name: '微信支付'},
                            {value: data.alipay, name: '支付宝支付'}
                        ],
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0,2 )'
                            }
                        }
                    }
                ]
            };
            chart.setOption(payChannelOption);
        }
    });
}

matches.change(function () {
    var matchId = $("#matches option:selected").val();
    if (matchId == "all") {
        init();
    } else {
        $.getJSON(baseUrl + "/orderStatistics", {matchId: matchId}, function (json) {
            if (json.meta.code == 200) {
                var data = json.data;

                var myChart = echarts.init(document.getElementById('pay'));
                var option = {
                    title: {
                        text: '支付情况',
                        subtext: '支付状态',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left',
                        data: ['已支付', '未支付', '正在支付中']
                    },
                    series: [
                        {
                            name: '',
                            type: 'pie',
                            radius: '55%',
                            center: ['50%', '60%'],
                            data: [
                                {value: data.isPay, name: '已支付'},
                                {value: data.isNotPay, name: '未支付'},
                                {value: data.isPaying, name: '正在支付中'}
                            ],
                            itemStyle: {
                                emphasis: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }
                    ]
                };
                myChart.setOption(option);

                var chart = echarts.init(document.getElementById('money'));
                var payChannelOption = {
                    title: {
                        text: '',
                        subtext: '支付渠道',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left',
                        data: ['微信支付', '支付宝支付']
                    },
                    series: [
                        {
                            name: '',
                            type: 'pie',
                            radius: '55%',
                            center: ['50%', '60%'],
                            data: [
                                {value: data.wxpay, name: '微信支付'},
                                {value: data.alipay, name: '支付宝支付'}
                            ],
                            itemStyle: {
                                emphasis: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0,2 )'
                                }
                            }
                        }
                    ]
                };
                chart.setOption(payChannelOption);
            }
        });
    }


});