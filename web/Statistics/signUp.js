var pageIndex = 1;
var pageSize = 10;
var pageTotal = 0;
var tbody = $("#signUp_list");
$(function () {
    pageIndex = 1;
    AjaxGetMatches(pageIndex, pageSize);
});
function AjaxGetMatches(index, size) {
    $.ajax({
        url: baseUrl + "/matches",
        type: "GET",
        data: {page: index - 1, size: size},
        dataType: "json",
        success: function (json) {
            var recordCount = json.pagination.nextMinId;
            pageTotal = Math.ceil(recordCount / size);
            $(".currentPage").text("第" + index + "页");
            $(".total").text("共" + pageTotal + "页");
            var data = json.data;
            tbody.empty();
            $.each(data, function (i, match) {
                var tr = "";

                tr += "<tr id='" + match.id + "'>";

                tr += "<td>";
                tr += match.name;
                tr += "</td>";

                tr += "<td>";
                tr += match.count;
                tr += "</td>";

                tr += "</tr>";
                tbody.append(tr);
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("请求数据失败");
        }
    });
}

function GoToFirstPage() {
    pageIndex = 1;
    AjaxGetMatches(pageIndex, pageSize);
}

function GoToPrePage() {
    pageIndex -= 1;
    pageIndex = pageIndex >= 1 ? pageIndex : 1;
    AjaxGetMatches(pageIndex, pageSize);
}

function GoToNextPage() {
    if (pageIndex < pageTotal) {
        pageIndex += 1;
    }
    AjaxGetMatches(pageIndex, pageSize);
}

function GoToEndPage() {
    pageIndex = pageTotal;
    AjaxGetMatches(pageIndex, pageSize);
}

function GoToAppointPage() {
    var page = $(".num>input").val();
    if (isNaN(page)) {
        alert("格式为数字类型!");
    }
    else {
        var tempPageIndex = pageIndex;
        pageIndex = parseInt($(".num>input").val());
        if (pageIndex <= 0 || pageIndex > pageTotal) {
            pageIndex = tempPageIndex;
            alert("请输入正确页码!");
        }
        else {
            AjaxGetMatches(pageIndex, pageSize);
        }
    }
}
