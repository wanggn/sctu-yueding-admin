var year = $("#year");
$(function(){

    var myChart = echarts.init(document.getElementById('main'));
    option = {
        title: {
            text: '注册人数统计图'
        },
        tooltip: {
            trigger: 'axis'
        },
//			legend: {
//				data:['邮件营销']
//			},
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
        },
        yAxis: {
            type: 'value'
        },
        series: [
            {
                name: '注册人数',
                type: 'line',
                stack: '总量',
                data: [120, 132, 101, 134, 90, 230, 210, 1322, 1031, 1034, 900, 1230]
            }
        ]
    };
    myChart.setOption(option);

});