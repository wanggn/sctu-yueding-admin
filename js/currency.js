/* 通用方法 */

    function confirm_submitAll(url,msg){
        layer.confirm(msg, {
            btn: ['确定','取消'] //按钮
        }, function(){
            submitForm(url,$('#myform'));
        }, function(){

        });
    }


    function confirm_deleteAll(url){
        layer.confirm('你确认删除吗？', {
            btn: ['确定','取消'] //按钮
        }, function(){
            submitForm(url,$('#myform'));
        }, function(){

        });
    }

    //ajax提交表单
    function submitForm(url,form){
        $.post(url,form.serialize(),function(data){
            if(data.status != -1 && data.status != 0){
                layer.alert(data.msg,{icon:1,time:2000,end:function(){
                    window.location.reload();
                }});
            }
            else if(data.status == -1 ){
                layer.alert(data.msg,{icon:2,time:2000,end:function(){
                    window.location.reload();
                }});
            }
            else if(data.status == 0 ){
                layer.alert(data['info'],{icon:2,time:2000,end:function(){
                    window.location.reload();
                }});
            }
        })
    }

    //进行操作并提示，status不为-1则提示消息
    function doAction(url){
        $.get(url,{},function(data){
            if(data.status != -1 && data.status != 0){
                layer.alert(data.msg,{icon:1,time:2000,end:function(){
                    window.location.reload();
                }});
            }
            else if(data.status == -1 ){
                layer.alert(data.msg,{icon:2,time:2000,end:function(){
                    window.location.reload();
                }});
            }
            else if(data.status == 0 ){
                layer.alert(data['info'],{icon:2,time:2000,end:function(){
                    window.location.reload();
                }});
            }
        })
    }

    //询问并进行某个操作
    function confirmurl(url,msg){
        layer.confirm(msg, {
            btn: ['确定','取消'] //按钮
        }, function(){
            doAction(url);
        }, function(){
        });
    }

    //修改的iframe
    function edit(url,name,area){
        if(area=='undefined' || area==undefined)
            area = ['893px', '600px']
        layer.open({
            type: 2,
            title: '修改--'+name,
            shadeClose: false,
            shade: 0.4,
            maxmin: false, //开启最大化最小化按钮
            area: area,
            content: url,
            btn: ['确定', '取消'],
            yes: function(index, layero){ //或者使用btn1
                var body = layer.getChildFrame('body', index);
                var form =  body.find('#dosubmit');
                form.click();
                return false;
            },
            cancel: function(index){ //或者使用btn2
                layer.closeAll();
            },
        });
    }

    //添加的弹出iframe
    function add(url,title,area){
        if(area=='undefined' || area==undefined)
            area = ['893px', '600px']
        layer.open({
            type: 2,
            title: title,
            shadeClose: false,
            shade: 0.4,
            maxmin: false, //开启最大化最小化按钮
            area: area,
            content: url,
            btn: ['确定', '取消'],
            yes: function(index, layero){ //或者使用btn1
                var body = layer.getChildFrame('body', index);
                var form =  body.find('#dosubmit');
                form.click();
                return false;
            },
            cancel: function(index){ //或者使用btn2
                layer.closeAll();
            },
        });
    }

    /**
     * 全选checkbox,注意：标识checkbox id固定为为check_box
     * @param string name 列表check名称,如 uid[]
     */
    function selectall(name) {
        if ($("#check_box").attr("checked")==false) {
            $("input[name='"+name+"']").each(function() {
                this.checked=false;
            });
        } else {
            $("input[name='"+name+"']").each(function() {
                this.checked=true;
            });
        }
    }

	/*高亮*/
	$(function(){
        $("#so_option").hover(function(){
            $(".soTypeList").show();
        });

        $("#so_option").hover(function(){
            $(".soTypeList").show();
        },function(){
            $(".soTypeList").hide();
        });

        $(".soTypeList li").click(function(){
            $("#so_type").html($(this).html());
            $(".soTypeList").hide();
        });
        /* 左边菜单高亮 */
        var $subnav = $("#accordion"),url;
        url = window.location.pathname + window.location.search;
        url = url.replace(/(\/(p)\/\d+) (&p=\d+) (\/(id)\/\d+) (&id=\d+) (\/(group)\/\d+) (&group=\d+)/, "");
        $subnav.find("a[href='" + url + "']").parent().addClass("current");

    });
$(function() {
	$.formValidator.initConfig( {
		formid: "myform", autotip: true, onerror: function(msg, obj) {
			window.top.art.dialog( {
				content: msg, lock: true, width: '200', height: '50'
			}
			, function() {
				this.close();
				$(obj).focus();
			}
			)
		}
	}
	);
	$("#channelstr").formValidator( {
		onshow: "请输入类别名称", onfocus: "请输入类别名称", oncorrect: "输入正确"
	}
	).inputValidator( {
		min: 1, onerror: "请输入类别名称"
	});
}) 