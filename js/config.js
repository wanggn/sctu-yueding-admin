var fileUrl = "http://118.24.106.163:8080/yueding-service/api/v1/files";
var baseUrl = "http://118.24.106.163:8080/yueding-service/api/v1/admin";
var clientUrl = "http://118.24.106.163:8080/yueding-service/api/v1";
var v2 = "http://118.24.106.163:8080/yueding-service/api/v2/admin";

//时间戳转换日期格式
function format(timestamp) {
    var timeStamp = parseInt(timestamp);
    var time = new Date(timeStamp);
    var y = time.getFullYear();
    var m = time.getMonth() + 1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    var s = time.getSeconds();
    return y + '-' + add0(m) + '-' + add0(d) + ' ' + add0(h) + ':' + add0(mm) + ':' + add0(s);
}

//时间戳转换日期格式
function formatToYYYY_MM_DD(timestamp) {
    var timeStamp = parseInt(timestamp);
    var time = new Date(timeStamp);
    var y = time.getFullYear();
    var m = time.getMonth() + 1;
    var d = time.getDate();
    return y + '-' + add0(m) + '-' + add0(d);
}
function add0(m) {
    return m < 10 ? '0' + m : m
}
